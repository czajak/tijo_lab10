package pl.edu.pwsztar.domain.board;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

// Valid Bishop move, if the piece moves from (sourceX, sourceY) to (destinationX, destinationY), the move is valid if and only if |destinationX-sourceX|=|destinationY-sourceY|.
@AllArgsConstructor
public class Bishop {
    public boolean isMoveValid(Position2D source, Position2D destination){
        return Math.abs(destination.getX() - source.getX()) == Math.abs(destination.getY() - source.getY());
    }
}
