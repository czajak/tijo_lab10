package pl.edu.pwsztar.domain.board;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public class ChessController {
    private PositionConverter pc = new PositionConverter();
    private FigureMoveDto figureMoveDto;
    private Bishop bishop;

    public ChessController(FigureMoveDto figureMoveDto){
        this.figureMoveDto = figureMoveDto;
        this.bishop = new Bishop();
    }

    public boolean validateMove(){
        return bishop.isMoveValid(pc.convert(figureMoveDto.getSource()),
                            pc.convert(figureMoveDto.getDestination()));
    }

}
