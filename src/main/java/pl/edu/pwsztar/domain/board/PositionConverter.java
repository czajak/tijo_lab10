package pl.edu.pwsztar.domain.board;

import java.util.HashMap;
import java.util.Map;

public class PositionConverter {
    Map<String, Integer> letters = new HashMap<>() {{
        put("a", 1);
        put("b", 2);
        put("c", 3);
        put("d", 4);
        put("e", 5);
        put("f", 6);
        put("g", 7);
        put("h", 8);
    }};

    public Position2D convert(String chessPosition){
        String[] coordinates = chessPosition.split("_");

        return new Position2D(
                letters.get(coordinates[0]),
                Integer.parseInt(coordinates[1]));
    }
}
