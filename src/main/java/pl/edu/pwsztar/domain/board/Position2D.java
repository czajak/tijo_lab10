package pl.edu.pwsztar.domain.board;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Position2D {
    @Setter
    @Getter
    private int x;
    @Setter
    @Getter
    private int y;
}
